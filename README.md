# Liliput

LiLiput is a collection of tools for inferring the structure of Linux
lists, presented in the paper "The Impact of Generic Data Structures:
Decoding the Role of Lists in the Linux Kernel" by Nic Volanschi and
Julia Lawall at ASE 2020.

If you ever wondered, Liliput stands for: Linux Lists program
understanding toolset.

## Files

The following directories are included in this archive:
- src/ contains the source of the Liliput tools
- results/ contains the results of running the tools, which can 
be rebuilt using the tools
- manual/ contains the results of manually analyzing a couple
of samples cited in the paper, related to comments attached to lists,
and unused lists, respectively

## Usage

Firstly, you might just browse the results of the tools under
results/.

Secondly, you can re-run only the type inference tool, starting from
the provided list usage reports under reports/lists/:

    cd results
    make cleantypes
    make runtypes

Thirdly, if you want to rebuild more results, you need some
dependencies installed on your system:

- OCaml (https://ocaml.org)
- Coccinelle (http://coccinelle.lip6.fr)
- Linux kernel sources (https://www.kernel.org)
- git (https://git-scm.com)
- Latex (https://www.latex-project.org)
- pdftk (https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)
- Graphviz (https://graphviz.org)

In this case, first compile the tools:

    cd src
    make

Then, run the tools using the Makefile under results, as follows.

__NB: Several of the commands below for reproducing results may take a
long time to execute.__

To reproduce the collection of comments:

    cd results
    make cleancomments
    make runcomments

Note that you will have to edit the Makefile variables SPATCH and
LINUXGIT to point to the Coccinelle binary and to your local Linux
source git repository, respectively.

To reproduce the construction of the list usage report:

    cd results
    make cleanlists
    make runlists

Runall has some command line options, for indicating the location of
Coccinelle (spatch), the Linux kernel source code (which must be a
local git clone), and the number of cores to use. More information is
available by running ./runall --help.

To produce some extractions of the lists usage report:

    cd results
    make cleanmissing
    make runmissing

To reproduce the graphs visualising different kinds of list types:

    cd results
    make cleandot
    make rundot
