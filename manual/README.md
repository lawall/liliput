This directory contains the results of manually inspecting some list
names samples in v5.6:

* File ManualInspectionComments.xlsx compares, for a sample of list
names, the typing information provided by the attached comments (when
present) with the typing information inferred by Liliput.

* File ManualInspectionUnused.xlsx checks, for a sample of list
names defined as global variables in C files, reported as being unused
by Liliput, whether they are really unused or not.
