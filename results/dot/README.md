This directory contains vizualization of some type graphs in v5.6:

- graphs_v5.6.pdf: graphs visualizing all list types

- hiddev.pdf: clusters containing a specific structure or field name
  (here struct hiddev)

- edge-to-head.pdf: only clusters with edge to head

- self-loops.pdf: only clusters with self loops

- non-self-loops.pdf: only clusters with edge to head that is not a self loop
