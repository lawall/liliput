# Liliput results

The subdirectories contain sample of results produced by Liliput tools,
as follows.

comments/ contains reports about comments attached to list names.

lists/ contains sample list usage reports.

types/ contains sample type reports, i.e. the type inferred for list
names.

dot/ contains sample graphs output by the list visualisation tool.

missing/ contains extractions from list usage reports concerning
problematic calls to a specific list API function (e.g. list_add).

Finally, the Makefile provides example for running the various tools
to (re)produce some of the results.
