# Liliput sources

This directory contains the sources of the Liliput toolset.

## List usage tool

The list usage report tool is implemented in Coccinelle in file
lists.cocci.

For running it, you need OCaml, Coccinelle, and the kernel sources.

For convenience, the reports produced by the tool are provided
under lists/ for all kernel versions between v3.0 and v5.6.

## Type inference tool

The type inference tool is implemented in Perl in file type.pl.

You don't need anything special to run it, except a standard Perl
installation.

The typing tool takes a list usage report as input, and produces a
type report on stdout, and some statistics on stderr.

The type report contains only lines of the form:

 listname : listname1 | listname2 | ...

where listname can be one of: 

 - structname.fieldname
 - __variable__.varname@functionname
 - __variable__.varname@path/filename

respectively representing: a structure field, a local variable within
a function, and a global variable in a C file (.c) or in a header file
(.h).

The meaning of the above line is that listname is a list head
containing elements of one of the types listname1, listname2, etc.

The statistics are in CSV form, and include counts and percentages for
different list categories (typed, untyped, etc.). See the paper for
more information.

## List comments tool

The list comments tool find comments attached to list names.
It is implemented in Coccinelle in file comments.cocci. For running it,
you need OCaml, Coccinelle, and the kernel sources.

For convenience, the result of the tool for kernel v4.19 are provided
under comments/comments.out

## List usage report explorator

The explorator for list usage reports is implemented in OCaml in file
get_missing.ml. It takes a list API function name (e.g. list_add) and a
list usage report, and outputs all the calls to this function where at
least one of the arguments could not be determined statically.  The
call is a hypertext link to a precise location in the kernel sources.

This tool is useful to detect cases which are not yet well covered
by Liliput.

## List types vizualization tool

The list types vizualization tool allows to build a graphical representation
of list types, in order to quickly understand different related list names.
It is implemented in file cluster.ml. 

The tool offers command options to vizualize only some specific
clusters, such as the types involving a specific structure or field
name, the types containing self-loops, etc.  For all the available
options, run it as follows:

    cluster --help

## Driver

Two of the above tools (the list usage tool and the list
comments tool) can be invoked by a driver implemented in file runall.ml
on all the stable releases of the Linux kernel contained in a given
kernel source repository. 

This driver takes as an argument a Coccinelle patch to invoke (e.g.
lists.cocci or comments.cocci). It also offers command options to
specify where to find the kernel sources, the Coccinelle executable,
etc. For more information on the available options, run it as follows:

    runall --help

## Usage

For compiling the tools, simply invoke 'make' in this directory.

For examples of using the tools, see the Makefile in the results/ directory,
whose usage is documented in the top-level README file.
