// spatch.opt -j 44 lists.cocci ~/linux --include-headers-for-types --include-headers --recursive-includes --very-quiet --no-show-diff > lists.out

#spatch -j 44 --include-headers-for-types --include-headers --recursive-includes --no-show-diff --no-safe-expressions --relax-include-path -I /run/shm/linux -I /run/shm/linux/include --very-quiet

virtual after_start

@initialize:ocaml@
@@

let debug = false
let debug2 = false

let max_structs = ref ([] : ((int * int) * (int * int)) list)

type info = AMBIGUOUS | OP of string * int * (Coccilib.pos*int)

type kind =
    FN of string (* name *) * string (* file *) * int (* line *)
  | VAR of string option (* file *) | NOTHING
  | STR of string (* name *) * string option (* file *)
  | STR2 of string (* name *) * string (* field *) * string option (* file *)
  | LOCAL

let clean file =
  match Str.bounded_split (Str.regexp "/linux/") file 2 with
    [_;aft] -> aft
  | _ -> file

let k2c ty fld =
  match ty with
    NOTHING -> fld
  | FN(ty,file,line) -> "__variable__." ^ fld^ "@" ^ ty
  | STR(ty,Some file) -> ty ^ "." ^ fld ^ "@" ^ (clean file)
  | STR(ty,None) -> ty ^ "." ^ fld
  | STR2(ty,f,Some file) -> ty ^ "." ^ f ^ "." ^ fld ^ "@" ^ (clean file)
  | STR2(ty,f,None) -> ty ^ "." ^ f ^ "." ^ fld
  | VAR(Some file) -> "__variable__." ^ fld ^ "@" ^ (clean file)
  | VAR None -> "__variable__." ^ fld
  | LOCAL -> "__local__"

let decls = Hashtbl.create 101
let structs = Hashtbl.create 101
let validstructs = Hashtbl.create 101
let ptbl = Hashtbl.create 101

let dirok file =
  match Str.bounded_split (Str.regexp_string "linux") file 2 with
    [_;file] ->
      (match Str.split_delim (Str.regexp "/") file with
	_::top::_ -> not(List.mem top ["tools";"scripts"])
      | _ -> true)
  | _ -> true

let init info p =
  if p = [] || dirok (List.hd p).file
  then
    begin
      (match info with
	(STR(i,None),fld) ->
	  let p = List.hd p in
	  Common.hashadd structs i (p.file,p.line,[[fld]],[info])
      | (STR2(i,f,None),fld) ->
	  let p = List.hd p in
	  Common.hashadd structs i (p.file,p.line,[[f;fld]],[info])
      | _ -> ());
      (try
	let cell = Hashtbl.find decls info in
	(if debug then Printf.eprintf "init: ambiguous: %s\n" (k2c (fst info) (snd info)));
	cell := [AMBIGUOUS]
      with Not_found ->
	begin
	  (if debug then Printf.eprintf "init: adding: %s\n" (k2c (fst info) (snd info)));
	  Hashtbl.add decls info (ref [])
	end)
    end

let reorganize_structs _ =
  Hashtbl.iter
    (fun str fields ->
      let fields = List.sort compare !fields in
      let rec loop = function
	  [] -> []
	| (file,line,fld,info)::(file2,line2,fld2,info2)::rest
	  when file = file2 && line = line2 ->
	    (* keep sorted but inefficient *)
	    loop ((file,line,List.append fld fld2,List.append info info2)::rest)
	| x::xs -> x :: loop xs in
      let structures = loop fields in
      let (_,_,first,infos) = List.hd structures in
      let ok =
	List.for_all
	  (fun (file,line,flds,_) ->
	    (if debug
	    then
	      Printf.eprintf "str: %s flds: %s first: %s\n"
		str (Dumper.dump flds) (Dumper.dump first));
	    flds = first)
	  (List.tl structures) in
      (if debug then Printf.eprintf "%s is ok? %b\n" str ok);
      if ok
      then
	begin
	  List.iter
	    (fun info ->
	      if Hashtbl.mem decls info
	      then failwith "should not be present"
	      else Hashtbl.add decls info (ref []))
	    infos;
	  Hashtbl.add validstructs str ()
	end)
    structs	    

let init2 info = (* called from finalize *)
  let update _ =
  try
    let cell = Hashtbl.find decls info in
    (if debug then Printf.eprintf "init2: ambiguous: %s\n" (k2c (fst info) (snd info)));
    cell := [AMBIGUOUS]
  with Not_found ->
    begin
      (if debug then Printf.eprintf "init2: adding: %s\n" (k2c (fst info) (snd info)));
      Hashtbl.add decls info (ref [])
    end in
  match info with
    (STR(i,None),fld)
  | (STR2(i,_,None),fld) ->
      if not (Hashtbl.mem validstructs i) then update()
  | _ -> update()

let get_current p =
  let p = List.hd p in
  FN(p.current_element,p.file,p.current_element_line)

let mkstr str i p =
  let file = (List.hd p).file in
  if Filename.extension file = ".c"
  then STR(i,Some file)
  else STR(i,None)

let mkstr2 str i f p =
  let file = (List.hd p).file in
  if Filename.extension file = ".c"
  then STR2(i,f,Some file)
  else STR2(i,f,None)

let mkvar p =
  let file = (List.hd p).file in
  if Filename.extension file = ".c"
  then VAR(Some file)
  else VAR None

let _ = init (NOTHING,"_") []

let check from ty fld =
  (if debug2 then Printf.eprintf "checking from %s: %s\n" from (k2c ty fld));
  try
    let cell =
      try Hashtbl.find decls (ty,fld)
      with Not_found ->
	(match ty with
	  STR(ty,Some _) -> Hashtbl.find decls (STR(ty,None),fld)
	| STR2(ty,f,Some _) -> Hashtbl.find decls (STR2(ty,f,None),fld)
	| VAR(Some _)  -> Hashtbl.find decls (VAR None,fld)
	| _ -> raise Not_found) in
    match !cell with
      [AMBIGUOUS] -> ((if debug2 then Printf.eprintf "ambiguous\n"); false)
    | _ -> ((if debug2 then Printf.eprintf "ok\n"); true)
  with Not_found -> ((if debug2 then Printf.eprintf "unknown\n"); false)

let check2 from local_ty global_ty fld =
  (* for identifiers, because LIST_HEAD declared ones aren't seen as local *)
  check from local_ty fld || check from global_ty fld

let update ty fld fn n sz p =
  if ty = LOCAL || ty = NOTHING || check fn ty fld
  then
  begin
  let ty =
    if ty <> LOCAL
    then
      begin
	let (cell,ty) =
	  try (Hashtbl.find decls (ty,fld),ty)
	  with Not_found ->
	    (match ty with
	      STR(ty,Some _) ->
		let ty = STR(ty,None) in
		(Hashtbl.find decls (ty,fld),ty)
	    | STR2(ty,f,Some _) ->
		let ty = STR2(ty,f,None) in
		(Hashtbl.find decls (ty,fld),ty)
	    | VAR(Some _)  ->
		let ty = VAR None in
		(Hashtbl.find decls (ty,fld),ty)
	    | _ -> failwith (Printf.sprintf "missing info: %s" (Dumper.dump ty))) in
	(if !cell <> [AMBIGUOUS] then cell := OP(fn,n,(p,sz)) :: !cell);
	ty
      end
    else ty in
  Common.hashadd ptbl (p,sz) (n,ty,fld)
  end

let update2 local_ty global_ty fld fn n sz p =
  (* for identifiers, because LIST_HEAD declared ones aren't seen as local *)
  if check fn local_ty fld
  then update local_ty fld fn n sz p
  else update global_ty fld fn n sz p

let reinit k v =
  let cell = Hashtbl.find decls k in
  cell := v :: !cell

let iota fr til =
  let rec loop n =
    if n > til then [] else n :: loop (n+1) in
  loop fr

let inafunction p = (* hack *)
  let p = List.hd p in
  p.current_element_line < p.line &&
  p.current_element != "something_else"

let toplevel p =
  let line = (List.hd p).line in
  List.exists (function ((l,_),_) -> line = l) !max_structs

(* -------------------------------------------------------------------- *)
(* step 1: collect type information *)

@r0a depends on !after_start@
identifier x;
position p : script:ocaml() { inafunction p };
@@

(
struct list_head x@p;
|
struct list_head x@p = ...;
)

@script:ocaml@
x << r0a.x;
p << r0a.p;
@@

init ((get_current p),x) p

@r0ar depends on !after_start@
identifier x;
position p : script:ocaml() { inafunction p };
@@

(
struct list_head x@p[...];
|
struct list_head x@p[...] = ...;
)

@script:ocaml@
x << r0ar.x;
p << r0ar.p;
@@

init ((get_current p),x) p




@r0a1 depends on !after_start disable optional_storage@
identifier x;
position p : script:ocaml() { not(inafunction p) };
@@

(
struct list_head x@p;
|
struct list_head x@p = ...;
)

@script:ocaml@
x << r0a1.x;
p << r0a1.p;
@@
init (VAR None,x) p

@r0ar2 depends on !after_start disable optional_storage@
identifier x;
position p : script:ocaml() { not(inafunction p) };
@@

(
struct list_head x@p[...];
|
struct list_head x@p[...] = ...;
)

@script:ocaml@
x << r0ar2.x;
p << r0ar2.p;
@@
init (VAR None,x) p

@r0ar3 depends on !after_start disable optional_storage@
identifier x;
position p : script:ocaml() { not(inafunction p) };
@@

(
struct list_head *x@p;
|
struct list_head *x@p = ...;
)

@script:ocaml@
x << r0ar3.x;
p << r0ar3.p;
@@
init (VAR None,x) p


@r0a4 depends on !after_start disable optional_storage@
identifier x;
position p : script:ocaml() { not(inafunction p) };
@@

(
static struct list_head x@p;
|
static struct list_head x@p = ...;
)

@script:ocaml@
x << r0a4.x;
p << r0a4.p;
@@
let file = (List.hd p).file in
if Filename.extension file = ".c"
then init (VAR(Some file),x) p
else init (VAR None,x) p

@r0ar5 depends on !after_start disable optional_storage@
identifier x;
position p : script:ocaml() { not(inafunction p) };
@@

(
static struct list_head x@p[...];
|
static struct list_head x@p[...] = ...;
)

@script:ocaml@
x << r0ar5.x;
p << r0ar5.p;
@@
let file = (List.hd p).file in
if Filename.extension file = ".c"
then init (VAR(Some file),x) p
else init (VAR None,x) p

@r0ar6 depends on !after_start disable optional_storage@
identifier x;
position p : script:ocaml() { not(inafunction p) };
@@

(
static struct list_head *x@p;
|
static struct list_head *x@p = ...;
)

@script:ocaml@
x << r0ar6.x;
p << r0ar6.p;
@@
let file = (List.hd p).file in
if Filename.extension file = ".c"
then init (VAR(Some file),x) p
else init (VAR None,x) p



@r0b depends on !after_start disable optional_storage@
identifier x;
declarer name LIST_HEAD;
position p : script:ocaml() { not(inafunction p) };
@@

LIST_HEAD(x@p);

@script:ocaml@
x << r0b.x;
p << r0b.p;
@@

init (VAR None,x) p

@r0cs depends on !after_start@
identifier x;
position p : script:ocaml() { not(inafunction p) };
declarer name LIST_HEAD;
@@ // top level

static LIST_HEAD(x@p);

@script:ocaml@
x << r0cs.x;
p << r0cs.p;
@@
let file = (List.hd p).file in
if Filename.extension file = ".c"
then init (VAR(Some file),x) p
else init (VAR None,x) p

@r0c depends on !after_start@
identifier x;
position p : script:ocaml() { inafunction p };
declarer name LIST_HEAD;
@@ // top level

LIST_HEAD(x@p);

@script:ocaml@
x << r0c.x;
p << r0c.p;
@@

init ((get_current p),x) p

@needed1@
@@

struct list_head

@needed2@
@@

LIST_HEAD(...);

@r0 depends on !after_start && (needed1 || needed2)@
identifier x;
position p,pe;
@@

struct x@p { ... }@pe

@script:ocaml depends on !after_start && r0@
@@
max_structs := []

@script:ocaml depends on !after_start@
p << r0.p;
pe << r0.pe;
@@

let s = ((List.hd p).line,(List.hd p).col) in
let se = ((List.hd pe).line,(List.hd pe).col) in
let (surround,other) =
  List.partition (function (c,ce) -> c < s && se < ce) !max_structs in
let (surrounded,other) =
  List.partition (function (c,ce) -> s < c && ce < se) other in
max_structs :=
  (match surround with
    [] -> (s,se)::other
  | _ -> List.append surround other)

@r1 depends on !after_start@
identifier x;
identifier s;
position p;
@@

(
struct x@p { ... struct list_head s; ... }
|
struct x@p { ... struct { ... struct list_head s; ... }; ... }
|
struct x@p { ... union { ... struct list_head s; ... }; ... }
|
struct x@p { ... union { ... struct { ... struct list_head s; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct list_head s; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... struct { ... struct list_head s; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... struct list_head s; ... }; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... union { ... struct list_head s; ... }; ... }; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... union { ... struct { ... struct list_head s; ... }; ... }; ... }; ... }; ... }; ... }; ... }
)

@script:ocaml@
x << r1.x;
s << r1.s;
p << r1.p;
@@

if toplevel p then
init (mkstr "r1" x p,s) p

@r1s depends on !after_start@
identifier x,s,f,i;
position p;
@@

(
struct x@p { ... struct { ... struct list_head s; ... } f; ... }
|
struct x@p { ... struct i { ... struct list_head s; ... } f; ... }
|
struct x@p { ... struct { ... struct list_head s; ... } f[...]; ... }
|
struct x@p { ... struct i { ... struct list_head s; ... } f[...]; ... }
|
struct x@p { ... union { ... struct list_head s; ... } f; ... }
|
struct x@p { ... union i { ... struct list_head s; ... } f; ... }
|
struct x@p { ... union { ... struct list_head s; ... } f[...]; ... }
|
struct x@p { ... union i { ... struct list_head s; ... } f[...]; ... }
)

@script:ocaml@
x << r1s.x;
s << r1s.s;
f << r1s.f;
p << r1s.p;
@@

if toplevel p then
init (mkstr2 "r1" x f p,s) p

@r1a depends on !after_start@
identifier x;
identifier s;
position p;
@@

(
struct x@p { ... struct list_head s[...]; ... }
|
struct x@p { ... struct { ... struct list_head s[...]; ... }; ... }
|
struct x@p { ... union { ... struct list_head s[...]; ... }; ... }
|
struct x@p { ... union { ... struct { ... struct list_head s[...]; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct list_head s[...]; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... struct { ... struct list_head s[...]; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... struct list_head s[...]; ... }; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... union { ... struct list_head s[...]; ... }; ... }; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... union { ... struct { ... struct list_head s[...]; ... }; ... }; ... }; ... }; ... }; ... }; ... }
)

@script:ocaml@
x << r1a.x;
s << r1a.s;
p << r1a.p;
@@

if toplevel p then
init (mkstr "r1a" x p,s) p

@r1as depends on !after_start@
identifier x,s,f,i;
position p;
@@

(
struct x@p { ... struct { ... struct list_head s[...]; ... } f; ... }
|
struct x@p { ... struct i { ... struct list_head s[...]; ... } f; ... }
|
struct x@p { ... struct { ... struct list_head s[...]; ... } f[...]; ... }
|
struct x@p { ... struct i { ... struct list_head s[...]; ... } f[...]; ... }
|
struct x@p { ... union { ... struct list_head s[...]; ... } f; ... }
|
struct x@p { ... union i { ... struct list_head s[...]; ... } f; ... }
|
struct x@p { ... union { ... struct list_head s[...]; ... } f[...]; ... }
|
struct x@p { ... union i { ... struct list_head s[...]; ... } f[...]; ... }
)

@script:ocaml@
x << r1as.x;
s << r1as.s;
f << r1as.f;
p << r1as.p;
@@

if toplevel p then
init (mkstr2 "r1" x f p,s) p


@r1b depends on !after_start@
identifier x;
identifier s;
position p;
@@

(
struct x@p { ... struct list_head *s; ... }
|
struct x@p { ... struct { ... struct list_head *s; ... }; ... }
|
struct x@p { ... union { ... struct list_head *s; ... }; ... }
|
struct x@p { ... union { ... struct { ... struct list_head *s; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct list_head *s; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... struct list_head *s; ... }; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... union { ... struct list_head *s; ... }; ... }; ... }; ... }; ... }; ... }
|
struct x@p { ... union { ... struct { ... union { ... struct { ... union { ... struct { ... struct list_head *s; ... }; ... }; ... }; ... }; ... }; ... }; ... }
)

@script:ocaml@
x << r1b.x;
s << r1b.s;
p << r1b.p;
@@
if toplevel p then
init (mkstr "r1b" x p,s) p

@r1bs depends on !after_start@
identifier x,s,f,i;
position p;
@@

(
struct x@p { ... struct { ... struct list_head *s; ... } f; ... }
|
struct x@p { ... struct i { ... struct list_head *s; ... } f; ... }
|
struct x@p { ... struct { ... struct list_head *s; ... } f[...]; ... }
|
struct x@p { ... struct i { ... struct list_head *s; ... } f[...]; ... }
|
struct x@p { ... union { ... struct list_head *s; ... } f; ... }
|
struct x@p { ... union i { ... struct list_head *s; ... } f; ... }
|
struct x@p { ... union { ... struct list_head *s; ... } f[...]; ... }
|
struct x@p { ... union i { ... struct list_head *s; ... } f[...]; ... }
)

@script:ocaml@
x << r1bs.x;
s << r1bs.s;
f << r1bs.f;
p << r1bs.p;
@@

if toplevel p then
init (mkstr2 "r1" x f p,s) p

@finalize:ocaml depends on !after_start@
ds << merge.decls;
ss << merge.structs;
@@

Hashtbl.clear decls;
List.iter (Hashtbl.iter (fun k v -> List.iter (Common.hashadd structs k) !v)) ss;
reorganize_structs();
List.iter
  (function d ->
     Hashtbl.iter (fun (x,s) _ -> init2 (x,s)) d)
  ds;

let it = new iteration() in
it#add_virtual_rule After_start;
(*Inc.set_parsing_style Inc.Parse_really_all_includes;*)
it#register()

(* -------------------------------------------------------------------- *)

@r2 depends on after_start disable ptr_to_array@
identifier i;
struct i *s;
identifier fn != {list_prepare_entry, list_prev_entry, list_next_entry, list_safe_reset_next, list_entry, container_of};
expression list[n] es;
expression list[m] ees;
position p;
identifier fld;
@@

fn@p(es,\(&(s->fld)\|&(s->fld[...])\|s->fld\|s->fld[...]\),ees)

@script:ocaml depends on after_start@
i << r2.i;
fld << r2.fld;
fn << r2.fn;
n << r2.n;
m << r2.m;
p << r2.p;
@@
update (mkstr "r2" i p) fld fn n (n+m+1) (List.hd p)

@r2b depends on after_start disable ptr_to_array@
identifier fn != {list_prepare_entry, list_prev_entry, list_next_entry, list_safe_reset_next, list_entry, container_of};
expression list[n] es;
expression list[m] ees;
position p;
global idexpression x;
@@

fn@p(es,\(&x\|&x[...]\),ees)

@script:ocaml depends on after_start@
x << r2b.x;
fn << r2b.fn;
n << r2b.n;
m << r2b.m;
p << r2b.p;
@@
if check2 fn (get_current p) (mkvar p) x then
update2 (get_current p) (mkvar p) x fn n (n+m+1) (List.hd p)

@r2c depends on after_start disable ptr_to_array@
identifier fn != {list_prepare_entry, list_prev_entry, list_next_entry, list_safe_reset_next, list_entry, container_of};
expression list[n] es;
expression list[m] ees;
position p;
local idexpression x;
@@

fn@p(es,\(&x\|&x[...]\),ees)

@script:ocaml depends on after_start@
x << r2c.x;
fn << r2c.fn;
n << r2c.n;
m << r2c.m;
p << r2c.p;
@@
update (get_current p) x fn n (n+m+1) (List.hd p)

@r3 depends on after_start disable fld_to_ptr@
identifier i;
struct i s;
identifier fn != {list_prepare_entry, list_prev_entry, list_next_entry, list_safe_reset_next, list_entry, container_of};
expression list[n] es;
expression list[m] ees;
position p;
identifier fld;
@@

fn@p(es,\(&(s.fld)\|&(s.fld[...])\|s.fld\|s.fld[...]\),ees)

@script:ocaml depends on after_start@
i << r3.i;
fld << r3.fld;
fn << r3.fn;
n << r3.n;
m << r3.m;
p << r3.p;
@@
update (mkstr "r3" i p) fld fn n (n+m+1) (List.hd p)

(* -------------------------------- *)

@s2 depends on after_start disable ptr_to_array@
identifier i;
struct i *s;
identifier fn != {list_prepare_entry, list_prev_entry, list_next_entry, list_safe_reset_next, list_entry, container_of};
expression list[n] es;
expression list[m] ees;
position p;
identifier f,fld;
@@

fn@p(es,\(&(s->f.fld)\|&(s->f.fld[...])\|s->f.fld\|s->f.fld[...]\|&(s->f[...].fld)\|&(s->f[...].fld[...])\|s->f[...].fld\|s->f[...].fld[...]\),ees)

@script:ocaml depends on after_start@
i << s2.i;
fld << s2.fld;
fn << s2.fn;
f << s2.f;
n << s2.n;
m << s2.m;
p << s2.p;
@@
update (mkstr2 "r2" i f p) fld fn n (n+m+1) (List.hd p)

@s3 depends on after_start disable fld_to_ptr@
identifier i;
struct i s;
identifier fn != {list_prepare_entry, list_prev_entry, list_next_entry, list_safe_reset_next, list_entry, container_of};
expression list[n] es;
expression list[m] ees;
position p;
identifier f,fld;
@@

fn@p(es,\(&(s.f.fld)\|&(s.f.fld[...])\|s.f.fld\|s.f.fld[...]\|&(s.f[...].fld)\|&(s.f[...].fld[...])\|s.f[...].fld\|s.f[...].fld[...]\),ees)

@script:ocaml depends on after_start@
i << s3.i;
fld << s3.fld;
fn << s3.fn;
f << s3.f;
n << s3.n;
m << s3.m;
p << s3.p;
@@
update (mkstr2 "r3" i f p) fld fn n (n+m+1) (List.hd p)

(* -------------------------------- *)
(* macros *)

@rle1 depends on after_start disable ptr_to_array, fld_to_ptr@
identifier i2,fld2,f,fn;
struct list_head e1;
struct i2 *e2;
struct i2 e3;
position p;
@@

 \(\(container_of@p\|list_entry@p\)\&fn\)
   (\(e1 \&
   \(e2->fld2\|e3.fld2\|e2->f.fld2\|e3.f.fld2\|e2->f[...].fld2\|e3.f[...].fld2\)\).\(next\|prev\),...)

@script:ocaml depends on after_start@
i << rle1.fn;
i2 << rle1.i2;
fld2 << rle1.fld2;
f << rle1.f = "__invalid__";
p << rle1.p;
@@
if f = "__invalid__" then
(try update (mkstr "rle1" i2 p) fld2 i 0 3 (List.hd p) with Not_found -> ())
else
(try update (mkstr2 "rle1" i2 f p) fld2 i 0 3 (List.hd p) with Not_found -> ())

@rle2 depends on after_start disable ptr_to_array, fld_to_ptr@
identifier i2,fld2,f,fn;
struct list_head *e1;
struct i2 *e2;
struct i2 e3;
position p;
@@

 \(\(container_of@p\|list_entry@p\)\&fn\)
   (\(e1 \&
   \(&(e2->fld2)\|&(e3.fld2)\|e2->fld2\|e3.fld2\|&(e2->fld2[...])\|&(e3.fld2[...])\|
           &(e2->f.fld2)\|&(e3.f.fld2)\|e2->f.fld2\|e3.f.fld2\|&(e2->f.fld2[...])\|&(e3.f.fld2[...])\|
           &(e2->f[...].fld2)\|&(e3.f[...].fld2)\|e2->f[...].fld2\|e3.f[...].fld2\|&(e2->f[...].fld2[...])\|&(e3.f[...].fld2[...])\)\)->\(next\|prev\),...)

@script:ocaml depends on after_start@
i << rle2.fn;
i2 << rle2.i2;
fld2 << rle2.fld2;
f << rle2.f = "__invalid__";
p << rle2.p;
@@
if f = "__invalid__" then
(try update (mkstr "rle2" i2 p) fld2 i 0 3 (List.hd p) with Not_found -> ())
else
(try update (mkstr2 "rle2" i2 f p) fld2 i 0 3 (List.hd p) with Not_found -> ())




@r6 depends on after_start disable ptr_to_array, fld_to_ptr exists@
iterator i = {list_for_each_safe,list_for_each_prev_safe,list_for_each,list_for_each_prev};
identifier i1,i2,fld1,fld2,f;
expression e1;
struct i2 *e2;
struct i2 e3;
global idexpression e4;
local idexpression e5;
struct i1 x;
expression list[n] e;
position p;
@@

i(e1, e, \(&(e2->fld2)\|&(e3.fld2)\|e2->fld2\|e3.fld2\|&(e2->fld2[...])\|&(e3.fld2[...])\|
           &(e2->f.fld2)\|&(e3.f.fld2)\|e2->f.fld2\|e3.f.fld2\|&(e2->f.fld2[...])\|&(e3.f.fld2[...])\|
           &(e2->f[...].fld2)\|&(e3.f[...].fld2)\|e2->f[...].fld2\|e3.f[...].fld2\|&(e2->f[...].fld2[...])\|&(e3.f[...].fld2[...])\|&e4\|&e5\)) { <...
(
  container_of@p(e1, \(struct i1\|typeof(x)\), fld1)
|
  list_entry@p(e1, \(struct i1\|typeof(x)\), fld1)
)
...> }

@script:ocaml depends on after_start@
i << r6.i;
i1 << r6.i1;
fld1 << r6.fld1;
_fld2 << r6.fld2;
p << r6.p;
n << r6.n;
@@
let mx = 3 + n - 1 in
(try update (mkstr "r6" i1 p) fld1 i 0 mx (List.hd p) with Not_found -> ());
List.iter
  (fun n ->
    try update NOTHING "_" i n mx (List.hd p) with Not_found -> ())
  (iota 1 (mx - 2))

@script:ocaml depends on after_start@
i << r6.i;
i2 << r6.i2;
fld2 << r6.fld2;
f << r6.f = "__invalid__";
p << r6.p;
n << r6.n;
@@
let mx = 3 + n - 1 in
if f = "__invalid__" then
(try update (mkstr "r6" i2 p) fld2 i (mx-1) mx (List.hd p) with Not_found -> ())
else
(try update (mkstr2 "r6" i2 f p) fld2 i (mx-1) mx (List.hd p) with Not_found -> ())

@script:ocaml depends on after_start@
i << r6.i;
i1 << r6.i1;
fld1 << r6.fld1;
e4 << r6.e4;
p << r6.p;
n << r6.n;
@@
let mx = 3 + n - 1 in
(try update (mkstr "r6" i1 p) fld1 i 0 mx (List.hd p) with Not_found -> ());
List.iter
  (fun n ->
    try update NOTHING "_" i n mx (List.hd p) with Not_found -> ())
  (iota 1 (mx - 2));
(try update2 (get_current p) (mkvar p) e4 i (mx-1) mx (List.hd p)
 with Not_found -> ())

@script:ocaml depends on after_start@
i << r6.i;
i1 << r6.i1;
fld1 << r6.fld1;
e5 << r6.e5;
p << r6.p;
n << r6.n;
@@
let mx = 3 + n - 1 in
(try update (mkstr "r6" i1 p) fld1 i 0 mx (List.hd p) with Not_found -> ());
List.iter
  (fun n ->
    try update NOTHING "_" i n mx (List.hd p) with Not_found -> ())
  (iota 1 (mx - 2));
(try update (get_current p) e5 i (mx-1) mx (List.hd p) with Not_found -> ())

@r6b depends on after_start disable ptr_to_array@
identifier i;
identifier fn;
expression list[n] es;
expression list[m] ees;
position p != r6.p;
identifier fld: script:ocaml(fn,i,p) { check fn (mkstr "r6b" i p) fld };
@@ // structure argument with a field in the next argument

fn@p(es,struct i,fld,ees)

@script:ocaml depends on after_start@
i << r6b.i;
fld << r6b.fld;
fn << r6b.fn;
n << r6b.n;
m << r6b.m;
p << r6b.p;
@@
update (mkstr "r6b" i p) fld fn n (n+m+2) (List.hd p)

@rlocal depends on after_start@
local idexpression struct list_head *x;
identifier fn;
expression list[n] es;
expression list[m] ees;
position p;
@@ // cheap solution for arrays

fn@p(es,x,ees)

@script:ocaml depends on after_start@
fn << rlocal.fn;
x << rlocal.x;
n << rlocal.n;
m << rlocal.m;
p << rlocal.p;
@@
(* see r6 *)
if not(List.mem fn ["container_of";"list_entry"])
then update LOCAL x fn n (n+m+1) (List.hd p)

@r7 depends on after_start disable ptr_to_array, fld_to_ptr@
iterator i = {list_for_each_entry_safe,list_for_each_entry_safe_continue,list_for_each_entry_safe_from,list_for_each_entry_safe_reverse,
list_for_each_entry,list_for_each_entry_reverse,list_for_each_entry_continue,list_for_each_entry_continue_reverse,list_for_each_entry_from_reverse};
identifier i1,i2,fld1,fld2,f;
struct i1 *e1;
struct i2 *e2;
struct i2 e3;
global idexpression e4;
local idexpression e5;
expression list[n] e;
statement S;
position p;
@@

i@p(e1, e, \(&(e2->fld2)\|&(e3.fld2)\|e2->fld2\|e3.fld2\|&(e2->fld2[...])\|&(e3.fld2[...])\|
             &(e2->f.fld2)\|&(e3.f.fld2)\|e2->f.fld2\|e3.f.fld2\|&(e2->f.fld2[...])\|&(e3.f.fld2[...])\|
             &(e2->f[...].fld2)\|&(e3.f[...].fld2)\|e2->f[...].fld2\|e3.f[...].fld2\|&(e2->f[...].fld2[...])\|&(e3.f[...].fld2[...])\|&e4\|&e5\), fld1) S

@script:ocaml depends on after_start@
i << r7.i;
i1 << r7.i1;
fld1 << r7.fld1;
_fld2 << r7.fld2;
p << r7.p;
n << r7.n;
@@

let mx = 3 + n - 1 in
(try update (mkstr "r7" i1 p) fld1 i 0 mx (List.hd p) with Not_found -> ());
List.iter
  (fun n ->
    try update NOTHING "_" i n mx (List.hd p) with Not_found -> ())
  (iota 1 (mx - 2))

@script:ocaml depends on after_start@
i << r7.i;
i2 << r7.i2;
fld2 << r7.fld2;
f << r7.f = "__invalid__";
p << r7.p;
n << r7.n;
@@

let mx = 3 + n - 1 in
if f = "__invalid__" then
(try update (mkstr "r7" i2 p) fld2 i (mx-1) mx (List.hd p) with Not_found -> ())
else
(try update (mkstr2 "r7" i2 f p) fld2 i (mx-1) mx (List.hd p) with Not_found -> ())

@script:ocaml depends on after_start@
i << r7.i;
i1 << r7.i1;
fld1 << r7.fld1;
e4 << r7.e4;
p << r7.p;
n << r7.n;
@@
let mx = 3 + n - 1 in
(try update (mkstr "r7" i1 p) fld1 i 0 mx (List.hd p) with Not_found -> ());
List.iter
  (fun n ->
    try update NOTHING "_" i n mx (List.hd p) with Not_found -> ())
  (iota 1 (mx - 2));
(try update2 (get_current p) (mkvar p) e4 i (mx-1) mx (List.hd p)
 with Not_found -> ())

@script:ocaml depends on after_start@
i << r7.i;
i1 << r7.i1;
fld1 << r7.fld1;
e5 << r7.e5;
p << r7.p;
n << r7.n;
@@
let mx = 3 + n - 1 in
(try update (mkstr "r7" i1 p) fld1 i 0 mx (List.hd p) with Not_found -> ());
List.iter
  (fun n ->
    try update NOTHING "_" i n mx (List.hd p) with Not_found -> ())
  (iota 1 (mx - 2));
(try update (get_current p) e5 i (mx-1) mx (List.hd p) with Not_found -> ())


@rilocal depends on after_start@
local idexpression struct list_head *x;
iterator i = {list_for_each_entry_safe,list_for_each_entry_safe_continue,list_for_each_entry_safe_from,list_for_each_entry_safe_reverse,
list_for_each_entry,list_for_each_entry_reverse,list_for_each_entry_continue,list_for_each_entry_continue_reverse,list_for_each_entry_from_reverse};
expression list[n] e;
expression list[m] e1;
statement S;
position p;
@@

i@p(e, x, e1) S

@script:ocaml depends on after_start@
i << rilocal.i;
x << rilocal.x;
p << rilocal.p;
n << rilocal.n;
m << rilocal.m;
@@

let mx = n+m+1 in
update LOCAL x i n mx (List.hd p)

@r8 depends on after_start disable ptr_to_array, fld_to_ptr@
identifier i1,i2,fld1,fld2,f;
struct i1 *e1;
struct i2 *e2;
struct i2 e3;
global idexpression e4;
local idexpression e5;
position p;
@@

list_prepare_entry@p(e1, \(&(e2->fld2)\|&(e3.fld2)\|e2->fld2\|e3.fld2\|&(e2->fld2[...])\|&(e3.fld2[...])\|
                           &(e2->f.fld2)\|&(e3.f.fld2)\|e2->f.fld2\|e3.f.fld2\|&(e2->f.fld2[...])\|&(e3.f.fld2[...])\|
                           &(e2->f[...].fld2)\|&(e3.f[...].fld2)\|e2->f[...].fld2\|e3.f[...].fld2\|&(e2->f[...].fld2[...])\|&(e3.f[...].fld2[...])\|&e4\|&e5\), fld1)

@script:ocaml depends on after_start@
i1 << r8.i1;
fld1 << r8.fld1;
_fld2 << r8.fld2;
p << r8.p;
@@

(try update (mkstr "r8" i1 p) fld1 "list_prepare_entry" 0 2 (List.hd p) with Not_found -> ())

@script:ocaml depends on after_start@
i2 << r8.i2;
fld2 << r8.fld2;
f << r8.f = "__invalid__";
p << r8.p;
@@
if f = "__invalid__" then
(try update (mkstr "r8" i2 p) fld2 "list_prepare_entry" 1 2 (List.hd p) with Not_found -> ())
else
(try update (mkstr2 "r8" i2 f p) fld2 "list_prepare_entry" 1 2 (List.hd p) with Not_found -> ())

@script:ocaml depends on after_start@
i1 << r8.i1;
fld1 << r8.fld1;
e4 << r8.e4;
p << r8.p;
@@
(try update (mkstr "r8" i1 p) fld1 "list_prepare_entry" 0 2 (List.hd p)
 with Not_found -> ());
(try update2 (get_current p) (mkvar p) e4 "list_prepare_entry" 1 2 (List.hd p)
 with Not_found -> ())

@script:ocaml depends on after_start@
i1 << r8.i1;
fld1 << r8.fld1;
e5 << r8.e5;
p << r8.p;
@@
(try update (mkstr "r8" i1 p) fld1 "list_prepare_entry" 0 2 (List.hd p) with Not_found -> ());
(try update (get_current p) e5 "list_prepare_entry" 1 2 (List.hd p) with Not_found -> ())

@r9 depends on after_start disable ptr_to_array, fld_to_ptr@
identifier i = {list_prev_entry,list_next_entry};
identifier i1,fld1;
struct i1 *e1;
position p;
@@

i@p(e1, fld1)

@script:ocaml depends on after_start@
i << r9.i;
i1 << r9.i1;
fld1 << r9.fld1;
p << r9.p;
@@
try update (mkstr "r9" i1 p) fld1 i 0 1 (List.hd p) with Not_found -> ()

@r10 depends on after_start disable ptr_to_array, fld_to_ptr@
identifier i = {list_safe_reset_next};
identifier i1,fld1;
struct i1 *e1;
expression e;
position p;
@@

i@p(e1, e, fld1)

@script:ocaml depends on after_start@
i << r10.i;
i1 << r10.i1;
fld1 << r10.fld1;
p << r10.p;
@@
try update (mkstr "r10" i1 p) fld1 i 0 1 (List.hd p) with Not_found -> ()

@finalize:ocaml depends on after_start@
ds << merge.decls;
ps << merge.ptbl;
@@

List.iter
  (function d ->
     Hashtbl.iter
      (fun (x,s) v ->
	List.iter (reinit (x,s)) !v) d)
  ds;

List.iter
  (function p ->
     Hashtbl.iter
      (fun p v ->
	List.iter (Common.hashadd ptbl p) !v) p)
  ps;

Hashtbl.iter
 (fun (p,sz) cell ->
   let v = List.sort compare !cell in
   let rec loop n = function
       (m,ty,fld)::rest ->
	 if n = m
	 then (m,ty,fld) :: loop (n+1) rest
	 else (n,NOTHING,"?") :: loop (n+1) ((m,ty,fld)::rest)
     | [] ->
	 if n = sz then [] else (n,NOTHING,"?") :: loop (n+1) [] in
   cell := loop 0 v)
  ptbl;

let infos =
  Hashtbl.fold (fun k v r -> (k,List.sort compare !v) :: r)
   decls [] in

List.iter
  (fun ((x,s), v) ->
    if not (List.mem AMBIGUOUS v)
    then
      begin
	Printf.printf "%s:\n" (k2c x s);
	List.iter
	  (function
	      OP(fn,offset,p) ->
		let args = !(Hashtbl.find ptbl p) in
		Printf.printf "   %s: %d: %s: %s:%d\n" fn offset
		  (String.concat ", " (List.map (fun (_,ty,fld) -> k2c ty fld) args))
		  (clean (fst p).file) (fst p).line
	    | _ -> failwith "not possible")
	  v
      end)
    (List.sort compare infos)
