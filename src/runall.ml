let linux = ref "/run/shm/linux"
let spatch = ref "spatch"
let sp = ref "comments.cocci"
let cores = ref 2

let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let versions _ =
  let cmd3 = Printf.sprintf "cd %s; git tag | grep v3 | grep -v rc" !linux in
  let cmd4 = Printf.sprintf "cd %s; git tag | grep v4 | grep -v rc" !linux in
  let cmd5 = Printf.sprintf "cd %s; git tag | grep v5 | grep -v rc" !linux in
  (cmd_to_list cmd3 @ cmd_to_list cmd4 @ cmd_to_list cmd5)

let run version sp =
  let cmd = Printf.sprintf "cd %s; git clean -dfx; git checkout -- .; git checkout master; git checkout %s" !linux version in
  ignore(Sys.command cmd);
  let out =
    (Filename.remove_extension(Filename.basename sp)) ^ version ^ ".out" in
  let res_file = Filename.temp_file "runall" ".res" in
  let cmd =
    Printf.sprintf "%s -j %d %s %s > %s 2> %s && cat %s"
      !spatch !cores sp !linux out res_file res_file in
  Printf.eprintf "running %s\n" cmd;
  let res = cmd_to_list cmd in
  Sys.remove res_file;
  res

let options =
  ["--spatch", Arg.Set_string spatch, "  path of spatch";
    "--linux", Arg.Set_string linux, "  path of the Linux kernel";
    "--cores", Arg.Set_int cores, "  number of cores"]

let anonymous s = sp := s

let usage = "runall sp.cocci [options]"

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  let versions = versions() in
  List.iter
    (function ver ->
      Printf.printf "Version: %s\n" ver;
      List.iter (fun x -> Printf.printf "  %s\n" x)
	(run ver !sp);
      Printf.printf "\n";
      flush stderr)
    versions
