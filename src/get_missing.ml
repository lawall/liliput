let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let get_lines re i =
  let clean re = String.concat ".*" (Str.split (Str.regexp_string "*") re) in
  let re = Str.regexp ("   "^(clean re)^":") in
  let res = ref [] in
  let rec loop _ =
    let x = input_line i in
    let ok =
      if Str.string_match re x 0
      then List.length (Str.split (Str.regexp_string "?") x) > 1
      else false in
    (if ok
    then
      let tp = Str.split (Str.regexp ": ") x in
      let args = List.nth tp 2 in
      let pieces = Str.split (Str.regexp ", ") args in
      let rec loop2 n = function
	  [] -> []
	| "?"::xs -> n :: loop2 (n+1) xs
	| x :: xs -> loop2 (n+1) xs in
      let info = List.nth tp 3 in
      match Str.split (Str.regexp ":") info with
	[file;line] -> res := (file,line,loop2 1 pieces) :: !res
      | _ -> failwith "bad info");
    loop() in
  try loop() with End_of_file -> List.rev !res

(* -------------------------------------------------- *)

let starter o =
  Printf.fprintf o "\\IfFileExists{preamble.tex}{\\input{preamble.tex}}{%%\n";
  Printf.fprintf o "\\documentclass[a4paper,article,oneside]{memoir}}\n";
  Printf.fprintf o "\\IfFileExists{usersetup.sty}{\\usepackage{usersetup}}{%%\n";
  Printf.fprintf o "\\usepackage{listings}\n";
  Printf.fprintf o "\\usepackage{hyperref}\n";
  Printf.fprintf o "\\usepackage{fullpage}\n";
  Printf.fprintf o "\\usepackage{memhfixc}\n";
  Printf.fprintf o "\\renewcommand{\\ttdefault}{pcr}\n";
  Printf.fprintf o "\\setsecnumdepth{part}\n";
  Printf.fprintf o "\\lstset{language=C,columns=fullflexible,basicstyle=\\ttfamily\\small,breaklines=tr\n";
  Printf.fprintf o "ue,xleftmargin=1cm,xrightmargin=1cm}\n";
  Printf.fprintf o "\\hypersetup{colorlinks=true}\n";
  Printf.fprintf o "\\pagestyle{plain}\n";
  Printf.fprintf o "\\advance\\hoffset by -1.75cm\n";
  Printf.fprintf o "\\textwidth 7.5in}\n";
  Printf.fprintf o "\\begin{document}\n"

let ender o =
  Printf.fprintf o "\\end{document}\n"

let clean s =
  String.concat "\\_" (Str.split (Str.regexp "_") s)

let data o (file,line,args) =
  let file = clean file in
  Printf.fprintf o "\\noindent\\href{https://elixir.bootlin.com/linux/v4.19/source/%s\\#L%s}{%s\\#L%s}\n\n"
    file line file line;
  Printf.fprintf o "Arguments: %s\n\n" (String.concat " " (List.map string_of_int args));
  Printf.fprintf o "\\vspace{\\baselineskip}\n\n"

(* -------------------------------------------------- *)

let _ =
  let re = Array.get Sys.argv 1 in
  let input = Array.get Sys.argv 2 in
  let i = open_in input in
  let infos = get_lines re i in
  close_in i;
  starter stdout;
  List.iter (data stdout) infos;
  ender stdout
