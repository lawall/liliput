let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let focus = ""

let hashadd tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  if not (List.mem v !cell) then cell := v :: !cell

(* --------------------------------------------------------------- *)

let file = ref "../cocci/lists_v5.6.out"
let output = ref ""
let tool = ref "../cocci/type.pl"
let loops = ref false
let self_loops = ref false
let non_self_loops = ref false
let required_name = ref ""

type info =
    StrF of string * string * string
  | Str of string * string
  | VarF of string * string
  | Var of string

let info2nlabel = function
    StrF(str,fld,file) -> fld
  | Str(str,fld) -> fld
  | VarF(v,file) -> v
  | Var v -> v

let info2label = function
    StrF(str,fld,file) ->
      Printf.sprintf "\"%s@%s\"" str (Filename.basename file)
  | Str(str,fld) -> "\""^str^"\""
  | VarF(v,file) -> Printf.sprintf "\"var@%s\"" (Filename.basename file)
  | Var v -> "var"

let info2c = function
    StrF(str,fld,file) ->
      Printf.sprintf "\"%s.%s@%s\"" str fld (Filename.basename file)
  | Str(str,fld) ->
      Printf.sprintf "\"%s.%s\"" str fld
  | VarF(v,file) ->
      Printf.sprintf "\"%s@%s\"" v (Filename.basename file)
  | Var v -> "\""^v^"\""

let nodes = Hashtbl.create 101
let edges = Hashtbl.create 101
let dests = Hashtbl.create 101

let parse_line str =
  match Str.split (Str.regexp_string ": ") str with
    [hd;elems] ->
      List.iter
	(function elem ->
	  let get_file s = Str.split (Str.regexp "@") s in
	  (* list_head prev or next doesn't make sense so ignore it *)
	  if not(hd = "list_head.prev" || hd = "list_head.next" ||
	         elem = "list_head.prev" || elem = "list_head.next")
	  then
	    begin
	      let var s =
		match Str.bounded_split (Str.regexp_string ".") s 2 with
		  ["__variable__";var] ->
		    (match get_file var with
		      [v] ->
			let res = Var v in
			hashadd nodes res res;
			(res,res)
		    | [v;file] ->
			let res = VarF (v,file) in
			hashadd nodes res res;
			(res,res)
		    | _ -> failwith "bad line1")
		| [str;fld] ->
		    (match get_file fld with
		      [v] ->
			let parent = Str(str,"") in
			let res = Str(str,v) in
			hashadd nodes parent res;
			(parent,res)
		    | [v;file] ->
			let parent = StrF(str,"",file) in
			let res = StrF(str,v,file) in
			hashadd nodes parent res;
			(parent,res)
		    | _ -> failwith ("bad line2: "^fld))
		| _ -> failwith ("bad line3: " ^ s) in
	      let (phd,hd) = var hd in
	      let (pelem,elem) = var elem in
	      hashadd edges hd elem;
	      hashadd dests elem ()
	    end)
	(Str.split (Str.regexp " | ") elems)
  | _ -> ()

let cluster _ =
  let clusters = Hashtbl.create 101 in
  Hashtbl.iter
    (fun parent nodes ->
      let pcluster =
	try Hashtbl.find clusters parent
	with Not_found -> [parent] in
      let nodes = !nodes in
      let targets =
	List.fold_left
	  (fun prev node ->
	    let dests =
	      try !(Hashtbl.find edges node)
	      with Not_found -> [] in
	    List.fold_left
	      (fun prev cur ->
		if List.mem cur prev then prev else cur::prev)
	      prev dests)
	  [] nodes in
      let ptargets =
	List.map
	  (function
	      Str(s,fld) -> Str(s,"")
	    | StrF(s,fld,file) -> StrF(s,"",file)
	    | x -> x)
	  targets in
      let pclusters =
	List.fold_left
	  (fun prev ptarget ->
	    let clus =
	      try Hashtbl.find clusters ptarget
	      with Not_found -> [ptarget] in
	    if List.mem clus prev then prev else clus::prev)
	  [] ptargets in
      let newcluster =
	let union l1 l2 =
	  List.fold_left
	    (fun prev cur ->
	      if List.mem cur prev then prev else cur::prev)
	    l1 l2 in
	List.fold_left union pcluster pclusters in
      List.iter
	(fun nd -> Hashtbl.replace clusters nd newcluster)
	newcluster)
    nodes;
  clusters

let ctr = ref 0

let paint clusters nd =
  let neighbors =
    try Hashtbl.find clusters nd
    with Not_found -> [] in
  if focus <> "" && not(List.mem focus (List.map info2label neighbors))
  then None
  else if neighbors = []
  then None
  else
    begin
      let edge_to_head = ref false in
      let self_loop = ref false in
      let edge_to_non_self = ref false in
      let required = ref (!required_name = "") in
      List.iter
	(fun nd ->
	  let nds = !(Hashtbl.find nodes nd) in
	  List.iter
	    (fun nd ->
	      (if not !required
	      then
		required :=
		  try
		    ignore(Str.search_forward (Str.regexp !required_name)
			     (info2c nd) 0);
		    true
		  with Not_found -> false);
	      (if Hashtbl.mem edges nd && Hashtbl.mem dests nd
	      then edge_to_head := true);
	      (try
		let self = Hashtbl.find edges nd in
		if List.mem nd !self then self_loop := true
	      with Not_found -> ());
	      (if Hashtbl.mem edges nd && Hashtbl.mem dests nd
	      then
		try
		  let self = Hashtbl.find edges nd in
		  if not(List.mem nd !self) then edge_to_non_self := true
		with Not_found -> ()))
	    nds;
	  Hashtbl.remove clusters nd)
	neighbors;
      let ok =
	!required &&
	if !loops
	then !edge_to_head
	else if !self_loops
	then !self_loop
	else if !non_self_loops
	then !edge_to_non_self
	else true in
      if ok
      then
	begin
	  let (fl,o) = Filename.open_temp_file "cluster" ".dot" in
	  Printf.fprintf o "digraph G {\n    rankdir=LR; compound=true;\n";
	  List.iter
	    (fun neighbor ->
	      let c = !ctr in
	      ctr := !ctr + 1;
	      Printf.fprintf o "  subgraph cluster%d {\n" c;
	      Printf.fprintf o "    label = %s;\n" (info2label neighbor);
	      let nds = !(Hashtbl.find nodes neighbor) in
	      List.iter
		(function nd ->
		  let ishead = Hashtbl.mem edges nd in
		  let lbl = info2nlabel nd in
		  Printf.fprintf o "    %s [label=\"%s\"%s];\n"
		    (info2c nd) lbl
		    (if ishead then ",color=blue,fontcolor=blue" else ""))
		nds;
	      Printf.fprintf o "    %s%s;\n"
		(String.concat " -> " (List.map info2c nds))
		(if List.length nds = 1 then "" else " [style=invis]");
	      Printf.fprintf o "  }\n")
	    neighbors;
	  List.iter
	    (fun neighbor ->
	      let nds = !(Hashtbl.find nodes neighbor) in
	      List.iter
		(fun nd ->
		  let dests =
		    try !(Hashtbl.find edges nd)
		    with Not_found -> [] in
		  List.iter
		    (fun dst ->
		      Printf.fprintf o "  %s -> %s;\n" (info2c nd) (info2c dst))
		    dests)
		nds)
	    neighbors;
	  Printf.fprintf o "}\n\n";
	  close_out o;
	  Some (List.length neighbors, fl)
	end
      else None
    end

(* ------------------------------------------------------------------- *)

let options =
  [ "-i", Arg.Set_string file, "  input file";
    "-o", Arg.Set_string output, "  output file";
    "--tool", Arg.Set_string tool, "  path to type.pl";
    "--required", Arg.Set_string required_name,"  required structure or field name";
    "--edge-to-head", Arg.Set loops, "  only clusters with edge to head";
    "--self-loops", Arg.Set self_loops, "  only clusters with self loops";
    "--non-self-edge-to-head", Arg.Set non_self_loops,
    "  only clusters with edge to head that is not a self loop"
  ]

let anonymous s = output := s

let usage = ""


let _ =
  Arg.parse (Arg.align options) anonymous usage;
  let lines = cmd_to_list (!tool ^ " " ^ !file) in
  List.iter parse_line lines;
  let clusters = cluster() in
  let files =
    Hashtbl.fold
      (fun nd _ r ->
	let fl = paint clusters nd in
	match fl with
	  Some (n,fl) -> (n,fl)::r
	| None -> r)
      nodes [] in
  let files = List.map snd (List.rev (List.sort compare files)) in
  let pdfs =
    List.map
      (function fl ->
	let pdf = (Filename.basename fl) ^ ".pdf" in
	ignore
	  (Sys.command
	     (Printf.sprintf "dot -T pdf %s > %s"
		fl pdf));
	pdf)
      files in
  (if !required_name = ""
  then ignore (Sys.command ("/bin/rm "^(String.concat " " files)))
  else Printf.printf "output in %s\n" (String.concat " " files));
  ignore
    (Sys.command
       (Printf.sprintf "pdftk %s cat output %s"
	  (String.concat " " pdfs) !output));
  ignore (Sys.command ("/bin/rm "^(String.concat " " pdfs)))
