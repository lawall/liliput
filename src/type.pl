#!/usr/bin/perl
my $USAGE = "type.pl usage-report";

use Getopt::Std;
our %opts = ();

getopts('rt', \%opts) or die $usage;

die $USAGE if $#ARGV != 0;
my $infile = $ARGV[0];
#print STDERR "open $infile\n";
open IN, "<$infile" or die


my %type = (); # the types of elements in the list of a head field:
my %refs = (); # the types referencing an element (inverse of %type)

my %elements; # set of all element types found in the lists above
my %fields; # number of calls for each field found
my %joined; # set of types which have been joined
my %le; # inequality constraints t1 <= t2

my ($str, $fld); # current struct & field names
while (<IN>) {
  chomp;
  if (/^\S/) { # field.struct line
    next if /^(list_head[.](prev|next))$/;
    ($str, $fld, $pos) = /^(\w+)[.]([\w.]+)(@[-\w\/.]+)?:$/;
    die "invalid struct.field name: $_" if !defined($fld);
    $fields{"$str.$fld$pos"} = 0;
  } else { # call line
    $fields{"$str.$fld$pos"}++;
    my ($fun, $n, $args, $file, $line) = /^\s*(\w+): (\d+): ([^:]*): ([^:]*):(\d+)$/; #*
    die "invalid call line" if !defined($args);
    my @args = split ", *", $args;
    #print "  call $fun/$n($args) with $#args args\n";
    die "invalid n-th arg" if $args[$n] ne "$str.$fld$pos";
    if (($fun =~ /^list_for_each_entry(_continue)?(_reverse)?$/ ||
          $fun =~ /^list_for_each(_prev)?$/ ||
          $fun =~ /^list_for_each_entry_from(_reverse)?$/) &&
        &is_type_pat($args[0]) && &is_type_pat($args[1])) {
      &type($args[1], $args[0]);
    } elsif (($fun =~ /^list_for_each_entry_safe(_from|_continue|_reverse)?$/ ||
              $fun =~ /^list_for_each(_prev)?_safe$/) &&
             &is_type_pat($args[0]) && &is_type_pat($args[2])) {
      &type($args[2], $args[0]);
    } elsif (($fun =~ /^list_first_entry(_or_null)?$/ ||
              $fun =~ /^list_last_entry$/)
            && &is_type_pat($args[0]) && &is_type_pat($args[1])) {
      &type($args[0], $args[1]);
    } elsif (($fun =~ /^list_entry$/ ||
              $fun =~ /^container_of$/)
            && &is_type_pat($args[0]) && &is_type_pat($args[1])) {
      # NB: it means that 1st arg was of these is of the form s.f.next/prev.
      # If both args are of the same type, probably visiting a neighbor element
      &type($args[0], $args[1]) if $args[0] ne $args[1];
    } elsif (($fun =~ /^list_is_(first|last)$/)
              && &is_type_pat($args[0]) && &is_type_pat($args[1])) {
      &type($args[1], $args[0]);
    } elsif (($fun =~ /^list_(add|move)(_tail)?$/ ||
              $fun =~ /^list_add(_tail)?_rcu$/)
              && &is_type_pat($args[0]) && &is_type_pat($args[1])) {
      &type($args[1], $args[0]); # if $args[1] ne $args[0];
    } elsif (($fun =~ /^list_splice(_tail)?(_init)?$/)
            && &is_type_pat($args[0]) && &is_type_pat($args[1])) {
      #&typeq($args[0], $args[1]);
      $le{$args[0]}{$args[1]} = 1; # if $args[0] ne $args[1];
    }
    # ------------------------------------------------------------------
    # Compensante the intra-procedural analysis with some frequent non-standard list functions:
      elsif (($fun =~ /^pci_free_resource_list|acpi_dev_free_resource_list|pci_add_resource(_offset)?$/)
            && &is_type_pat($args[0])) {
      &type($args[0], "resource_entry.node");
    } elsif (($fun =~ /^unregister_netdevice_many$/)
            && &is_type_pat($args[0])) {
      &type($args[0], "net_device.unreg_list");
    } elsif (($fun =~ /^vchan_dma_desc_free_list/)
            && &is_type_pat($args[1])) {
      &type($args[1], "virt_dma_desc.node");
    } elsif (($fun =~ /^vchan_get_all_descriptors/)
            && &is_type_pat($args[1])) {
      $le{"virt_dma_chan.desc_allocated"}{$args[1]} = 1;
      $le{"virt_dma_chan.desc_submitted"}{$args[1]} = 1;
      $le{"virt_dma_chan.desc_issued"}{$args[1]} = 1;
      $le{"virt_dma_chan.desc_completed"}{$args[1]} = 1;
      $le{"virt_dma_chan.desc_terminated"}{$args[1]} = 1;
    }
  }
}

print STDERR "cleaning up empty types ..." if $opts{'t'};
for my $x (keys %type) {
  if (keys %{$type{$x}} == 0) {
    delete $type{$x};
    delete $joined{$x};
    printf STDERR " $x";
  }
}
printf STDERR " done\n" if $opts{'t'};
print STDERR "cleaning up empty refs ..." if $opts{'t'};
for my $x (keys %refs) {
  if (keys %{$refs{$x}} == 0) {
    delete $refs{$x};
    printf STDERR " $x" if $opts{'t'};
  }
}
printf STDERR " done\n" if $opts{'t'};

my $nle = keys %le;
print STDERR "computing fixpoint of $nle 'le' constraints..." if $opts{'t'};
my $modified;
my $iter = 0;
do { # iterate until no modification
  $modified = 0;
  $iter++;
  for my $x (keys %le) {
    for my $y (keys %{$le{$x}}) {
      if ($x ne $y && exists $type{$x}) {
        # skips splices before/after a link in the same list as the head:
        next if exists $type{$x}{$y};
        for my $t (keys %{$type{$x}}) {
          if (!exists $type{$y}{$t}) {
            #printf STDERR "propagating type $t from $x to $y\n";
            $type{$y}{$t} = 1;
            $refs{$t}{$y} = 1;
            $modified = 1;
          }
        }
      }
    }
  }
} while ($modified);
print STDERR "done (after $iter iterations)\n" if $opts{'t'};

my $npost = 0;
if (exists($opts{'r'})) {
  print STDERR "computing reverse fixpoint of $nle constraints..." if $opts{'t'};
  $iter = 0;
  do { # iterate until no modification
    $modified = 0;
    $iter++;
    for my $x (keys %le) {
      for my $y (keys %{$le{$x}}) {
        if (!exists($type{$x}) && exists($type{$y}) && (keys %{$type{$y}}) == 1) {
          #printf STDERR "back-propagating type from $y to $x\n";
          my $t = (keys %{$type{$y}})[0];
          $type{$x}{$t} = 1;
          $refs{$t}{$x} = 1;
          $modified = 1;
          $npost++;
        }
      }
    }
  } while ($modified);
  print STDERR "done (after $iter iterations)\n" if $opts{'t'};
}

for my $k (sort keys %type) {
  &print_type($k);
}

print STDERR "cleaning up unused field with unique position ..." if $opts{'t'};
for my $x (sort keys %fields) {
  if ($fields{$x} == 0) {
    my @positions = grep(/^$x@/, (keys %fields));
    if (@positions == 1) {
      print "dropping empty section for $positions[0]\n" if $opts{'t'};
      delete $fields{$x};
    }
  }
}
print STDERR " done\n" if $opts{'t'};

my $nfields = keys %fields;
my $ntypes = keys %type;
my $nelems = keys %elements;
my $ntotal = $ntypes + $nelems;
my %union;
my $nundeclared = 0;
for $x (keys %type, keys %elements) {
  $union{$x} = 1;
  if (!exists $fields{$x}) {
    #print STDERR "undeclared field: $x\n";
    $nundeclared++;
  }
}
my $nunion = keys %union;
my $nself = 0;
my $nboth = 0;
my $notherboth = 0;
my $nmutual = 0;
my $nonlyheads = 0;
my $nonlyelems = 0;
#for $x (keys %elements) {
#  if (exists($type{$x})) {
for my $x (keys %fields) {
  if (exists($type{$x}) && exists($elements{$x})) {
    $nboth++;
    if ((keys %{$type{$x}}) == 1 && exists $type{$x}{$x}) {
      $nself++;
      #print STDERR "$x : [$type{$x}]\n";
    } else {
      my @ks = keys %{$type{$x}};
      if (@ks == 1) {
        my $y = $ks[0];
        if (exists($type{$y}) && (keys %{$type{$y}}) == 1 && exists($type{$y}{$x})) {
          $nmutual++;
          #print STDERR "mutual ref $x <-> $y\n" if $x lt $y;
          next;
        }
      }
      $notherboth++;
      #print STDERR "both element and type: ";
      #&print_type($x);
    }
  } elsif (exists($type{$x})) {
    $nonlyheads++;
  } elsif (exists($elements{$x})) {
    $nonlyelems++;
  }
}
#  }
#}
my $nuntyped = 0;
my $ntyped = 0;
my $nunused = 0;
my $nunused_but_typed = 0;
for $x (sort keys %fields) {
  if (exists $type{$x} || exists $elements{$x}) {
    $ntyped++;
    if ($fields{$x} == 0) {
      $nunused_but_typed++;
      print STDERR "unused BUT TYPED: $x\n" if $opts{'t'};
    }
  } else {
    $nuntyped++;
    if ($fields{$x} == 0) {
      $nunused++;
      #print STDERR "unused: $x\n";
    } else {
      #print STDERR "untyped: $x\n";
    }
  }
}
my $npolytyped = 0; #grep {ref $type{$_} == ARRAY} (keys %type);
for $x (keys %type) {
  if ((keys %{$type{$x}}) > 1) {
    $npolytyped++;
    #print STDERR "$x : " . (join " | ", keys %{$type{$x}}) . "\n";
  }
}
my $npolyref = 0;
for $x (keys %refs) {
  if ((keys %{$refs{$x}}) > 1) {
    $npolyref++;
    #&print_refs($x);
  }
}
# Look for trees: s.f : s.f', where f' != f
my $ntrees = 0;
for my $x (keys %type) {
  my ($s, $f) = ($x =~ /^(.*)[.](.*)$/);
  if (defined($f)){
    my @ks = keys %{$type{$x}};
    for my $y (@ks) {
      my ($s1, $f1) = ($y =~ /^(.*)[.](.*)$/);
      if (defined($f1) && $s eq $s1 && $f ne $f1) {
        $ntrees++;
        #print STDERR "found tree: $x : $y\n";
      }
    }
  }
}
if ($opts{'t'}) { # textual output
  print STDERR "$nfields lists found = $ntyped (" . &percent($ntyped / $nfields) .") typed";
  print STDERR " (incl. $nunused_but_typed unused)" if $nunused_but_typed != 0;
  print STDERR " + $nuntyped (" . &percent($nuntyped / $nfields) . ") untyped (= $nunused (" . &percent($nunused / $nfields) . ") unused + @{[$nuntyped-$nunused]} (" . &percent(($nuntyped - $nunused) / $nfields) . ") used)\n";
  my $score = ($ntyped + $nunused) / $nfields;
  printf STDERR "score = " . &percent($score) . "\n", $score;
  die if $nfields != $ntyped + $nuntyped;

  print STDERR "$ntyped typed fields = $nonlyheads (" . &percent($nonlyheads / $ntyped) . ") heads" .
    " + $nonlyelems (" . &percent($nonlyelems / $ntyped) . ") elements" .
    " + $nboth (" . &percent($nboth / $ntyped) . ") both head&elem\n";
  die if $ntyped != $nonlyheads + $nonlyelems + $nboth;
  print STDERR "$nboth both = $nself (" . &percent($nself / $nboth) .") self-lists".
    " + @{[$nmutual/2]} (" . &percent($nmutual / $nboth) . ") mutual pairs" .
    " + $notherboth (" . &percent($notherboth / $nboth) . ") other\n";
  die if $nboth != $nself + $nmutual + $notherboth;

  print STDERR "$ntypes heads (incl. $npolytyped polytyped) and $nelems elements (incl. $npolyref polyrefed)\n";
  #print STDERR "($ntotal total = $nunion distinct + $nself self-lists + $nmutual mutual pairs + $notherboth other both head&elem)\n";
  #die if $ntotal != $nunion + $nself + $nmutual + $nboth;
  my $njoined = keys %joined;
  print STDERR "$nundeclared undeclared fields, $njoined joined fields, $npost post-typed fields\n";
} else { # CSV output
  print STDERR "$nfields;lists\n$ntyped;typed;" . &percent($ntyped / $nfields) ."\n";
  print STDERR "$nunused_but_typed nunused_but_typed\n" if $nunused_but_typed != 0;
  #print STDERR "$nuntyped;untyped+unused;" . &percent($nuntyped / $nfields) . "\n";
  print STDERR "$nunused;unused;" . &percent($nunused / $nfields) . "\n";
  print STDERR $nuntyped-$nunused . ";untyped;" . &percent(($nuntyped - $nunused) / $nfields) . "\n";
  #my $score = ($ntyped + $nunused) / $nfields;
  #printf STDERR "score = " . &percent($score) . "\n", $score;
  die if $nfields != $ntyped + $nuntyped;

  #print STDERR "$ntyped;typed\n";
  print STDERR "$nonlyheads;heads;" . &percent($nonlyheads / $ntyped) . "\n" .
    "$nonlyelems;elements;" . &percent($nonlyelems / $ntyped) . "\n" .
    "$nboth;both;" . &percent($nboth / $ntyped) . "\n";
  die if $ntyped != $nonlyheads + $nonlyelems + $nboth;
  print STDERR "$nself;self-lists;" . &percent($nself / $nboth) ."\n".
    $nmutual/2 . ";mutual pairs;" . &percent($nmutual / $nboth) . "\n" .
    "$notherboth;other;" . &percent($notherboth / $nboth) . "\n";
  die if $nboth != $nself + $nmutual + $notherboth;

  print STDERR "$ntypes;heads+both\n$npolytyped;polytyped heads\n";
  print STDERR "$nelems;elements+both\n$npolyref;polyrefed elements\n";
  my $njoined = keys %joined;
  print STDERR "$nundeclared;undeclared\n$njoined;joined\n$npost;post-typed\n";
  print STDERR "$ntrees;trees\n"
}

# re-scan the input reports
seek IN, 0, 0;

#my %list_entry_on_head = ();
# my @init_list_head_on_element = ();
# while(<>) {
#   if (/^\S/) { # field.struct line
#     #($str, $fld, $pos) = /^(\w+)[.]([\w.]+)(@[-\w\/.]+)?:$/;
#   } else { # call line
#     my ($fun, $n, $args, $file, $line) = /^\s*(\w+): (\d+): ([^:]*): ([^:]*):(\d+)$/; #*
#     my @args = split ", *", $args;
#     # if ($fun eq "list_entry" && exists($type{$args[0]})) {
#     #   $list_entry_on_head{$args[0]}++;
#     # }
#     if ($fun eq "INIT_LIST_HEAD" && !exists($type{$args[0]}) && exists($elements{$args[0]}) ) {
#       print STDERR "err: $_";
#       push @init_list_head_on_element, $args[0];
#     }
#   }
# }
# print STDERR "$#init_list_head_on_element errors\n";
# for my $x (sort keys %list_entry_on_head) {
#   print STDERR "apply list_entry on head: $x\n";
# }
# print STDERR (keys %list_entry_on_head) . " list_entry(head) errors\n";

exit;


sub percent() {
  my ($f) = @_;
  return sprintf "%.1f%%", $f * 100;
}

sub print_type() {
  my ($x) = @_;
  print "$x: " . (join " | ", sort keys %{$type{$x}}) . "\n";
}

sub print_refs() {
  my ($x) = @_;
  print STDERR "$x <- " . (join " | ", keys %{$refs{$x}}) . "\n";
}

sub type() {
  my ($var, $typ) = @_;
  $type{$var}{$typ} = 1;
  $refs{$typ}{$var} = 1;
  $elements{$typ} = 1; # add to set of element types
}

sub typeq() {
  my ($x, $y) = @_;
  if (exists($type{$x})) {
    if (exists($type{$y})) {
      for my $t (keys %{$type{$y}}) { # add to tx all elements in ty
        $type{$x}{$t} = 1;
      }
      $type{$y} = $type{$x}; # unify types of x and y
    } else { # y is undef
      $type{$y} = $type{$x};
    }
  } else {
    if (exists($type{$y})) { # x is undef
      $type{$x} = $type{$y};
    } else { # x and y are undef
      $type{$x} = {};
      $type{$y} = $type{$x};
    }
  }
  for my $t (keys %{$type{$x}}) { # all elements in tx now refer to tx and ty
    $refs{$t}{$x} = 1;
    $refs{$t}{$y} = 1;
  }
  $joined{$x} = 1;
  $joined{$y} = 1;
  # no new elements to add
}

sub is_type_pat() {
  my ($pat) = @_;
  $pat !~ /^([?]|__local__|list_head[.](prev|next))$/
}
