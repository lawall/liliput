#spatch --include-headers --no-safe-expressions --no-includes --very-quiet

// a comment can appear on the line before the field, in which case the
// column offset of the comment an the field should be the same.

// a comment can appear on the same line as the field, in which case it
// should appear after the field

// we also check for comments that appear within the field, although these
// seem unlikely

@initialize:ocaml@
@@

let nothing = ref 0
let something = ref 0

let topcom = Hashtbl.create 101

let account f c cs p =
  let (cb,cm,ca) = List.hd c in
  let (cbs,_,cas) = List.hd cs in

  let line = (List.hd p).line in
  let col = (List.hd p).col in

  let comment_line ca = (Token_c.info_of_token ca).Common.line in
  let comment_col ca = (Token_c.info_of_token ca).Common.column in

  let before =
    match List.rev cb with
      x::xs ->
	let xpos = List.hd(List.rev cbs) in
	if comment_line xpos < line && comment_col xpos = col
	then Some x
	else None
    | _ -> None in

  let middle =
    if cm = [] then None else Some cm in

  let after =
    match ca with
      x::xs ->
	let xpos = List.hd cas in
	if comment_line xpos = line && comment_col xpos > col
	then Some x
	else None
    | _ -> None in

  let p = List.hd p in

  let outer_before =
    try
      let loc = (p.file,p.current_element_line,p.current_element_line_end) in
      let top = Hashtbl.find topcom loc in
      let ok =
	let pieces = Str.split (Str.regexp "\\b") top in
	List.mem f pieces in
      if ok then Some top else None
    with Not_found -> None in

  if before = None && middle = None && after = None && outer_before = None
  then nothing := !nothing + 1
  else
    begin
      something := !something + 1;
      Printf.printf "%s:%d: %s\n" p.file p.line f;
      (*Coccilib.print_main "" [p];*)
      (match before with
	Some x -> Printf.printf "before: %s\n" x
      | None -> ());
      (match middle with
	Some x ->
	  Printf.printf "middle:\n";
	  List.iter (fun c -> Printf.printf "%s\n" c) x
      | None -> ());
      (match after with
	Some x -> Printf.printf "after: %s\n" x
      | None -> ());
      (match outer_before with
	Some x -> Printf.printf "top: %s\n" x
      | None -> ())
    end

@needed@
@@

struct list_head

@script:ocaml depends on needed@
@@
Hashtbl.clear topcom

@topcom depends on needed@
identifier x;
position p;
comments c;
@@

(
struct@p@c x { ... };
|
union@p@c x { ... };
)

@script:ocaml@
c << topcom.c;
p << topcom.p;
@@

let (cb,cm,ca) = List.hd c in
match List.rev cb with
  c::_ ->
    let p = List.hd p in
    Hashtbl.add topcom
      (p.file,p.current_element_line,p.current_element_line_end)
      c
| _ -> ()

@r@
comments c;
field f;
identifier i,fld;
position p;
@@

struct i {
...
(
(
struct list_head fld;
|
struct list_head *fld;
|
struct list_head fld[...];
)
&
f@p@c
)
... }

@script:ocaml@
(c,cs) << r.c;
p << r.p;
f << r.fld;
@@

account f c cs p

@t@
comments c;
field f;
identifier fld;
position p;
@@

struct {
...
(
(
struct list_head fld;
|
struct list_head *fld;
|
struct list_head fld[...];
)
&
f@p@c
)
... }

@script:ocaml@
(c,cs) << t.c;
p << t.p;
f << t.fld;
@@

account f c cs p


@r1@
comments c;
field f;
identifier i,fld;
position p;
@@

union i {
...
(
(
struct list_head fld;
|
struct list_head *fld;
|
struct list_head fld[...];
)
&
f@p@c
)
... }

@script:ocaml@
(c,cs) << r1.c;
p << r1.p;
f << r1.fld;
@@

account f c cs p

@t1@
comments c;
field f;
identifier fld;
position p;
@@

union {
...
(
(
struct list_head fld;
|
struct list_head *fld;
|
struct list_head fld[...];
)
&
f@p@c
)
... }

@script:ocaml@
(c,cs) << t1.c;
p << t1.p;
f << t1.fld;
@@

account f c cs p




@finalize:ocaml@
n << merge.nothing;
s << merge.something;
@@

List.iter (fun n -> nothing := !nothing + !n) n;
List.iter (fun s -> something := !something + !s) s;
Printf.printf "nothing: %d\n" !nothing;
Printf.printf "something: %d\n" !something;
Printf.printf "commented: %d%%\n" ((100 * !something) / (!something + !nothing))
